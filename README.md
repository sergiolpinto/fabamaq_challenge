# fabamaq_challenge

# About the challenge and myself
These are my answers to the challenge proposed to me by Fabamaq.

I would like to say that all the subjects in this challenge are very new to me (I have never done web development, since my main experience focuses on Unity, virtual reality and augmented reality), and that I tackled them with a lot of research of all these topics, with some guidance and teaching from a good friend of mine, much more experienced in these fields, who spent the last week giving me some classes about javascript and HTML.

However, I gave it my best and tried to solve it by going to the basics and learning everything as well as I could, in the short amount of time I had to solve the proposed problems, and am open to learn so much more in the future, in order to improve my current skills.

# Exercise 1
The scripts and files for the first exercise can be found in this page.

I tried to tackle it by going to the basics and keeping my solution as simple as possible.

Not all of the requisites were fulfilled due to me being new to these fields, for example:
 - There are no sounds being played when the balls reach the destination or when a prime number is found, because I wasn't able to get libraries to work in my solution for the problem (to tackle the prime numbers found, there are logs being displayed in the console).
 - The play button does not pause and resume the animation, due mostly to lack of time to find a good way to keep the "current" position of each ball and calculating the correct velocity for the animation to keep the 3 seconds in total.

 # Exercise 2
The first code snippet consists in a function that was supposed to return the result of another function, in a string format. However, the way it was written does not do what it says it does, and instead the function returns the result of the "getAnimationTransform" function.


The second code snippet consists in a function that intends to return the text in "context.message.text". However, the way it is written makes it so that the "catch" portion does nothing. 

Explaining it further, if "context.message" is null, then the function does nothing; if "context.message" exists, but "context.message.text" is null, then the function also does nothing; finally, if both of them exist, then the function returns "context.message.text", meaning that the "return null" after "catch" never runs.

In order to have the intended functionality, a solution would be to delete both "try" and "catch", and just write "return null" after the "if", like this:  

```javascript
function context_output(context){
    if((context.message != null) && (context.message.text != null)){
            return (context.message.text)
        }   
    }

    return null
}
```

# Exercise 3
This function is implementing a kind of "loading" functionality, where the functions receive an initial and final values, and just count from the initial to the final progressively, according to the value of "params.frames" (which tells the function how long it should take to count).

When the function finishes (meaning that the final value has been achieved), then a "callback" is called.

Both variations of the function intend to do the same thing, however they are different in the optimization aspect, where the first variation is based on the percentage of where the function is, between the initial and final values, instead of running "callback" by checking the value of "currentValue".