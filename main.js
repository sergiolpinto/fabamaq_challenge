// Parent - the yellow area where the balls are moving
const BoardId = 'Board';
const board = document.getElementById(BoardId);

const numMaxBalls = 30;
const heightBetweenBalls = 8;
const dBall = 15;

// Splits the numMaxBalls (30) into three groups, with different colours
const ballsGroupNumber = {
  0: 'red',
  1: 'blue',
  2: 'green',
};

const maxMoveTime = 3000;
const distance = 300;
const frameTime = 5;

// The array that will assign the random numbers to the balls, assuring that there are no repeated numbers
const ballNumbers = [];

// Auxiliar function, returns a string with the position value and adds 'px', for pixels
function getPx(num) {
  return num + 'px';
}

// Gets the second digit of "index", dividing the balls into one of the colour groups
function getGroupNum(index) {
  return Math.floor((index / 10) % 10);
}

// Gets the random integer number between the limits (including)
function getRandomIntInclusive(min, max) {
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min + 1)) + min;
}

// Function that is responsible for finding the prime numbers, if there were any assigned to all the balls
function isPrime(num) {
  for (let i = 2; i < num; i++) if (num % i === 0) return false;
  return num > 1;
}

// Assigns the random integer number between 1 and 60 (including) to each of the balls, without repeating numbers
function getRandomBallNumber() {
  while (true) {
    const randomNumber = getRandomIntInclusive(1, 60);
    if (!ballNumbers.includes(randomNumber)) {
      ballNumbers.push(randomNumber);
      return randomNumber;
    }
  }
}

// Function that creates each ball, until the numMaxBall, and arranges them into the correct "hierarchy", as well as the correct position
function createBalls() {
  for (let index = 0; index < numMaxBalls; index++) {
    const ball = document.createElement('div');
    ball.setAttribute('id', index);
    ball.classList.add('ball');
    ball.style.backgroundColor = ballsGroupNumber[getGroupNum(index)];
    ball.style.height = getPx(dBall);
    ball.style.width = getPx(dBall);
    ball.style.top = getPx(index * (dBall + heightBetweenBalls));
    ball.style.left = getPx(0);

    const ballNumberText = document.createTextNode(getRandomBallNumber());
    ball.style.color = 'white';
    ball.appendChild(ballNumberText);

    board.appendChild(ball);
  }
}

// Moves each individual ball and controls its velocity. It also checks if the number assigned to that ball is prime or not.
function moveBall(ballId, velocity) {
  let id = null;
  const ball = document.getElementById(ballId);
  let ballPosition = 0;

  clearInterval(id);
  id = setInterval(frame, frameTime);
  function frame() {
    if (ballPosition >= distance) {
      const num = parseInt(ball.firstChild.nodeValue);

      if (isPrime(num)) {
        console.log('MAKE SOUND: PRIME NUMBER ' + num);
      }
      ball.style.left = distance;
      clearInterval(id);
    } else {
      ballPosition += velocity;
      ball.style.left = getPx(ballPosition);
    }
  }
}

// Responsible for assuring that the entire animation lasts 3 seconds, by assigning the velocity to the balls
function moveBalls() {
  for (let index = 0; index < numMaxBalls; index++) {
    const randomInterval = getRandomIntInclusive(500, 1000);
    const velocity = (frameTime * distance) / (maxMoveTime - randomInterval);

    setTimeout(function () {
      moveBall(index, velocity);
    }, randomInterval);
  }
}

// The Play button that starts and restarts the animation
const playButtonId = 'PlayButton';
const playButton = document.getElementById(playButtonId);

// The event that starts/restarts the animation and that checks for the pressing of the button
function playButtonEvent(event) {
  ballNumbers.length = 0;
  moveBalls();
}

createBalls();
playButton.addEventListener('click', playButtonEvent);
